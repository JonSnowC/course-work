import yaml


def read_yaml(file):
    with open(file, 'r', encoding='utf8') as f:
        content = yaml.safe_load(f)
        return content
