import pytest

from Pytest测试实战.python_code.calculator import Calculator

@pytest.fixture()
def calc_fixture():
    calc = Calculator()
    print('开始计算')
    yield calc
    print('结束计算')