import allure
import pytest
from Pytest测试实战.common.yaml_management import read_yaml


@allure.feature('计算器模块')
class TestCalculator(object):
    @pytest.mark.parametrize('num1,num2,expect', read_yaml('./data/calc.yaml')['add']['data'],
                             ids=read_yaml('./data/calc.yaml')['add']['ids'])
    @allure.story('加法计算')
    def test_add(self, num1, num2, expect, calc_fixture):
        with allure.step(f'计算{num1},{num2}的值相加'):
            result = calc_fixture.add(num1, num2)
        with allure.step('验证计算结果是否正确'):
            assert expect == result, f'测试结果不正确，正确的结果是{result}'

    @pytest.mark.parametrize('num1,num2,expect', read_yaml('./data/calc.yaml')['div']['data'],
                             ids=read_yaml('./data/calc.yaml')['div']['ids'])
    @allure.story('除法计算')
    def test_div(self, num1, num2, expect, calc_fixture):
        with allure.step(f'计算{num1},{num2}的值相除'):
            result = calc_fixture.div(num1, num2)
        with allure.step('开始验证计算结果是否正确'):
            assert expect == result, f'测试结果不正确，正确的结果是{result}'
