from mitmproxy import http

from 雪球Mock.file_management import read_file

import mitmproxy

from mitmproxy.tools.main import mitmdump


class HTTPEvents:

    def request(self, flow: mitmproxy.http.HTTPFlow):
        if 'https://stock.xueqiu.com/v5/stock/batch/quote.json' in flow.request.url and 'x=' in flow.request.url:
            data = read_file('quote.json')
            flow.response = http.Response.make(200, data, )
            print(flow.response.text)


addons = [HTTPEvents()]
if __name__ == '__main__':
    mitmdump(['-p', '8080', '-s', __file__])
