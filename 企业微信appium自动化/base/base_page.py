from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage(object):
    def __init__(self, driver: WebDriver):
        self.driver = driver

    def find(self, locator):
        ele = WebDriverWait(self.driver, 15).until(EC.presence_of_element_located(locator))
        return ele

    def swipe_find(self, locator, num=3):
        """
        滑动查找
        :param locator:要查找元素
        :param num:循环次数
        :return:返回查找的元素
        """
        i = 1
        while i <= num:
            try:
                return self.find(locator)
            except:
                print('没找到，滑动继续查找')
                size = self.driver.get_window_size()
                width = size['width']
                height = size['height']
                startx = width / 2
                starty = height * 0.8
                endx = startx
                endy = height * 0.3
                duration = 2000
                self.driver.swipe(startx, starty, endx, endy,duration)
                i += 1
                if i > num:
                    raise NoSuchElementException(f'循环找了{num},还是未找到元素')
