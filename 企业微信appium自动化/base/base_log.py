import logging


def log():
    logger = logging.getLogger('app.log')
    if not logger.handlers:
        logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    file_handler = logging.FileHandler('../log/applog.txt', encoding='utf-8',mode='a+')
    console_handler.setLevel(logging.DEBUG)
    file_handler.setLevel(logging.INFO)

    fomartor = logging.Formatter('%(levelname)-8s: %(asctime)s %(filename)s %(funcName)s %(msg)s ',
                                 datefmt='%Y-%m-%d %H:%M:%S')

    console_handler.setFormatter(fomartor)
    file_handler.setFormatter(fomartor)

    logger.addHandler(console_handler)
    logger.addHandler(file_handler)

    return logger

