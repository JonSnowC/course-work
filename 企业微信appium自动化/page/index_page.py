from appium.webdriver.common.mobileby import MobileBy

from 企业微信appium自动化.base.base_page import BasePage
from 企业微信appium自动化.page.contact_list_page import ContactListPage


class IndexPage(BasePage):
    __contact_ele = (MobileBy.XPATH,'//*[@text="通讯录"]')
    def goto_contact(self):
        # 点击通讯录
        self.find(self.__contact_ele).click()
        return ContactListPage(self.driver)

