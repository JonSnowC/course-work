from appium.webdriver.common.mobileby import MobileBy
from 企业微信appium自动化.base.base_page import BasePage



class EditMemberPage(BasePage):
    __name_ele = (MobileBy.ID,'com.tencent.wework:id/bd6')
    __tel_ele = (MobileBy.ID,'com.tencent.wework:id/g9e')
    __save_button = (MobileBy.XPATH,'//*[@text="保存"]')

    def edit_member(self,name,tel):
        self.find(self.__name_ele).send_keys(name)
        self.find(self.__tel_ele).send_keys(tel)
        self.find(self.__save_button).click()
        # 不然要循环导入
        from 企业微信appium自动化.page.add_member_page import AddMemberPage
        return AddMemberPage(self.driver)

