from appium.webdriver.common.mobileby import MobileBy

from 企业微信appium自动化.base.base_page import BasePage
from 企业微信appium自动化.page.edit_member_page import EditMemberPage


class AddMemberPage(BasePage):
    __manual_add_ele = (MobileBy.XPATH, '//*[@text="手动输入添加"]')
    __toast_ele = (MobileBy.XPATH,'//*[@text="添加成功"]')

    def goto_edit_member_page(self):
        # 滚动查找添加成员,然后点击添加成员
        self.find(self.__manual_add_ele).click()
        return EditMemberPage(self.driver)

    def verify_ok(self):
        return self.find(self.__toast_ele)
