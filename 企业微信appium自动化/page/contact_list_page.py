from appium.webdriver.common.mobileby import MobileBy

from 企业微信appium自动化.base.base_page import BasePage
from 企业微信appium自动化.page.add_member_page import AddMemberPage


class ContactListPage(BasePage):
    __add_member_ele = (MobileBy.XPATH, '//*[@text="添加成员"]')
    def goto_add_member_page(self):
        self.swipe_find(self.__add_member_ele).click()
        return AddMemberPage(self.driver)