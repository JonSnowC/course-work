import allure
import pytest

from 企业微信appium自动化.base.base_log import log
from 企业微信appium自动化.base.yaml_management import read_yaml
from 企业微信appium自动化.page.index_page import IndexPage


@allure.feature('通讯录模块')
@allure.story('成员管理')
class TestContact(object):
    @allure.title('添加成员，录入正确的必填项')
    @pytest.mark.parametrize('name,tel', read_yaml('../data/member.yaml')['success'])
    def test_edit_member(self, start, name, tel):
        index = IndexPage(start)
        with allure.step(f'添加成员，成员的姓名是{name},电话是{tel}'):
            try:
                log().info('断言成员是否添加成功')
                result = index.goto_contact().goto_add_member_page().goto_edit_member_page().edit_member(name,
                                                                                                         tel).verify_ok().text
                assert '添加成功' == result
            except:
                log().info('添加失败')
                index.driver.save_screenshot(f'../screenshot/{name}.png')
                allure.attach.file(f'../screenshot/{name}.png', name=name, attachment_type=allure.attachment_type.PNG)
                assert False,'断言失败，成员没有添加成功'

        with allure.step('回到主页'):
            index.driver.start_activity('com.tencent.wework', 'com.tencent.wework.launch.WwMainActivity')
