from appium import webdriver
import pytest

@pytest.fixture(scope='session')
def start():
    desired_capa = {}
    desired_capa['platformName']='Android'
    desired_capa['platformVersion']='6.0'
    desired_capa['deviceName']='mumu'
    desired_capa['appPackage']='com.tencent.wework'
    desired_capa['appActivity']='com.tencent.wework.launch.WwMainActivity'
    desired_capa['noReset']=True
    desired_capa['adbExecTimeout']=30000
    driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub',desired_capabilities=desired_capa)
    driver.implicitly_wait(10)
    yield driver
    driver.quit()




