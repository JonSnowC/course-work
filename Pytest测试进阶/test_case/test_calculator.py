import pytest
from Pytest测试进阶.common.yaml_management import read_yaml
from Pytest测试进阶.python_code.calculator import Calculator


class TestCalculator(object):
    def setup_class(self):
        self.calc = Calculator()
        print('开始计算')

    def teardown_class(self):
        print('结束计算')

    @pytest.mark.parametrize('num1,num2,expect', read_yaml('../data/calc.yaml')['add']['data'],
                             ids=read_yaml('../data/calc.yaml')['add']['ids'])
    def test_add(self, num1, num2, expect):
        result = self.calc.add(num1, num2)
        assert expect == result, f'测试结果不正确，正确的结果是{result}'

    @pytest.mark.parametrize('num1,num2,expect', read_yaml('../data/calc.yaml')['div']['data'],
                             ids=read_yaml('../data/calc.yaml')['div']['ids'])
    def test_div(self,num1,num2,expect):
        result = self.calc.div(num1, num2)
        assert expect == result, f'测试结果不正确，正确的结果是{result}'
