# 使用简单工厂方法， 实现timo 和 police 两个英雄
# 一个回合制游戏，有两个英雄，分别以两个类进行定义。分别是timo和police。每个英雄都有 hp 属性和 power属性，hp 代表血量，power 代表攻击力
# 每个英雄都有一个 fight 方法：
# my_hp = hp - enemy_power
# enemy_final_hp = enemy_hp - my_power
# 两个 hp 进行对比，血量剩余多的人获胜

# 每个英雄都一个speak_lines方法
# 调用speak_lines方法，不同的角色会打印（讲出）不同的台词
# timo : 提莫队长正在待命
# police: 见识一下法律的子弹


class Hero(object):
    def __init__(self, name, hp, power):
        """
        :param name:英雄名字
        :param hp: 英雄生命值
        :param power: 英雄攻击力
        """
        self.name = name
        self.hp = hp
        self.power = power

    def fight(self, enemy):
        """
        :param enemy:敌方英雄
        """
        my_hp = self.hp - enemy.power
        enemy_final_hp = enemy.hp - self.power
        if my_hp > enemy_final_hp:
            print('敌人输了')
        elif my_hp < enemy_final_hp:
            print('敌人赢了')
        else:
            print('打平了')

    def speak_lines(self, dialogue):
        print(f'{self.name}的经典台词是:{dialogue}')


# 实例化2个英雄
timo = Hero('提莫队长', 500, 100)
police = Hero('女警', 600, 80)

# 英雄对战 ==>提莫对战女警
timo.fight(police)  # 敌人赢了
timo.speak_lines('提莫队长正在待命')
police.speak_lines('见识一下法律的子弹')
