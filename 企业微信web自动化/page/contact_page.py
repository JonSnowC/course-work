from selenium.webdriver.common.by import By

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.base_page import BasePage


class ContactPage(BasePage):
    __names_ele = (By.CSS_SELECTOR, '.member_colRight_memberTable_td')
    __department_ele = (By.XPATH, '//ul//li[@role="treeitem"]')

    def goto_member(self):
        pass

    def goto_department(self):
        pass

    def check_member(self):
        log().info('获取列表成员的名称')
        ele_list = self.finds(self.__names_ele)
        name_list = [i.text for i in ele_list]
        return name_list

    def check_department(self):
        log().info('获取列表部门名称')
        ele_list = self.finds(self.__department_ele)
        # 有一个坑点，取出来的列表数据中，每一个数据包含了一个空格
        #[' 中国\n ada\n 222\n 666\n 行政部', ' ada', ' 222', ' 666', ' 行政部']
        department_list = [i.text for i in ele_list]
        # 去掉列表中元素的空格
        department_list = [i.strip() for i in department_list]
        # print(department_list)
        # print(type(department_list))
        return department_list
