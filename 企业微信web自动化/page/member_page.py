import time

from selenium.webdriver.common.by import By

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.base_page import BasePage
from 企业微信web自动化.page.contact_page import ContactPage


class MemberPage(BasePage):
    __user_name = (By.ID, 'username')
    __user_account = (By.ID, 'memberAdd_acctid')
    __TEL = (By.ID, 'memberAdd_phone')
    __button = (By.XPATH, '//a[text()="保存"]')
    __tbody_name = (By.CSS_SELECTOR, 'td:nth-child(2)')

    def add_member(self, username, account, tel):
        log().info(f'录入成员姓名:{username}')
        self.input_keys(self.__user_name, username)

        log().info(f'录入成员账户:{account}')
        self.input_keys(self.__user_account, account)

        log().info(f'录入成员电话:{tel}')
        self.input_keys(self.__TEL, tel)

        log().info('点击保存成员信息')
        self.click_button(self.__button)
        return ContactPage(self.driver)
