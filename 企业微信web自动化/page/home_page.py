from selenium.webdriver.common.by import By

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.base_page import BasePage
from 企业微信web自动化.page.import_member_page import ImportMemberPage

from 企业微信web自动化.page.member_page import MemberPage


class HomePage(BasePage):
    url = "https://work.weixin.qq.com/wework_admin/frame#index"
    __add_member_ele = (By.XPATH, '//span[text()="添加成员"]')
    __import_member_ele = (By.XPATH, '//span[text()="导入通讯录"]')

    def goto_member(self):
        log().info('点击首页上的添加成员按钮')
        self.click_button(self.__add_member_ele)
        return MemberPage(self.driver)

    def goto_import_member(self):
        log().info('点击首页上的导入通讯录按钮')
        self.click_button(self.__import_member_ele)
        return ImportMemberPage(self.driver)
