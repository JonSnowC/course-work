import time

from selenium.webdriver.common.by import By

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.base_page import BasePage
from 企业微信web自动化.page.contact_page import ContactPage


class DepartmentPage(BasePage):
    url = 'https://work.weixin.qq.com/wework_admin/frame#contacts'
    __click_button = (By.XPATH, '//span[@class="icon jstree-contextmenu-hover"]')
    __click_add_button = (By.XPATH, '//li//a[text()="添加子部门"]')
    __department_content = (By.XPATH, '//input[@class="qui_inputText ww_inputText"]')
    __save_button = (By.XPATH, '//a[text()="确定"]')

    def add_department(self, content):
        log().info('点击+')
        self.click_button(self.__click_button)
        log().info('点击添加子部门')
        self.click_button(self.__click_add_button)
        log().info(f'录入部门信息：{content}')
        self.input_keys(self.__department_content, content)
        log().info('保存')
        self.click_button(self.__save_button)
        time.sleep(1)
        return ContactPage(self.driver)
