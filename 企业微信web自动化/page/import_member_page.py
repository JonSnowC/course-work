import time

from selenium.webdriver.common.by import By

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.base_page import BasePage
from 企业微信web自动化.page.contact_page import ContactPage


class ImportMemberPage(BasePage):
    __upload_ele = (By.ID, 'js_upload_file_input')
    __confirm_button = (By.ID,'submit_csv')
    __look_ele = (By.ID,'reloadContact')

    def import_member(self, file):
        log().info('上传文件')
        self.input_keys(self.__upload_ele,file)
        log().info('点击确定上传')
        time.sleep(2)
        self.click_button(self.__confirm_button)
        log().info('点击前往查看')
        self.click_button(self.__look_ele)
        return ContactPage(self.driver)

