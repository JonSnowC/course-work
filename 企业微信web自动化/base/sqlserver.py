# coding=utf8
import pymssql
from configparser import ConfigParser


class TestDB(object):
    def __init__(self, config_file, env):
        conf = ConfigParser()
        conf.read(config_file)
        self.server = conf[env]['server']
        self.user = conf[env]['user']
        self.passwd = conf[env]['password']
        self.db = conf[env]['db']

    def __get_connect(self):
        self.conn = pymssql.connect(self.server, self.user, self.passwd, self.db, charset='cp936', as_dict=True)
        self.cursor = self.conn.cursor()
        return self.cursor

    def select_record(self, sql, code_format=None):
        self.__get_connect()
        self.cursor.execute(sql, code_format)
        result = self.cursor.fetchall()
        self.cursor.close()
        self.conn.close()
        return result

    def delete_record(self, sql,code_format=None):
        self.__get_connect()
        self.cursor.execute(sql,code_format)
        self.conn.commit()
        self.cursor.close()
        self.conn.close()


t1 = TestDB('./db.ini', 'dev')


