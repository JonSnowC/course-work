import logging


def log():
    logger = logging.getLogger('web.log')
    if not logger.handlers:
        logger.setLevel(logging.DEBUG)

        console_handler = logging.StreamHandler()
        file_handler = logging.FileHandler('../log/run.log', mode='w', encoding='utf-8')

        console_handler.setLevel(logging.DEBUG)
        file_handler.setLevel(logging.INFO)

        formater = logging.Formatter('%(levelname)-8s: %(asctime)s %(filename)s %(funcName)s %(msg)s ',
                                     datefmt='%Y-%m-%d %H:%M:%S')

        console_handler.setFormatter(formater)
        file_handler.setFormatter(formater)

        logger.addHandler(console_handler)
        logger.addHandler(file_handler)
    return logger
