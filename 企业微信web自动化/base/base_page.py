from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver

    def find(self, locator):
        ele = WebDriverWait(self.driver, 10).until(ec.presence_of_element_located(locator))
        return ele

    def finds(self, locator):
        ele = WebDriverWait(self.driver, 20).until(ec.presence_of_all_elements_located(locator))
        return ele

    # 输入方法
    def input_keys(self, locator, content):
        self.find(locator).send_keys(content)

    # 点击方法
    def click_button(self, locator):
        self.find(locator).click()

    # 切换frame
    def switch_frame(self, locator):
        self.driver.switch_to.frame(self.find(locator))

    # 打开网址
    def open_url(self):
        self.driver.get(self.url)
