import time

import allure

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.page.home_page import HomePage

@allure.feature('首页')
@allure.story('导入通讯录')
class TestImportMember(object):
    @allure.title('导入通讯录')
    def test_import_member(self, browser):
        home = HomePage(browser)
        home.open_url()
        with allure.step('上传通讯录模板，判断成员是否导入成功'):
            result = home.goto_import_member().import_member(
                'F:/course-work/企业微信web自动化/data/通讯录批量导入模板.xlsx').check_member()
            time.sleep(1)
            log().info('导入模块数据，断言导入是否成功')
            assert '周杰伦' in result
