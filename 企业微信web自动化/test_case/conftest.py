import pytest
from selenium import webdriver

from 企业微信web自动化.base.base_log import log


@pytest.fixture(scope='session')
def browser():
    log().info('复用浏览器')
    ops = webdriver.ChromeOptions()
    ops.add_argument('-no-sandbox')
    ops.debugger_address='localhost:9001'
    driver = webdriver.Chrome(options=ops)
    driver.implicitly_wait(10)
    driver.maximize_window()
    yield driver
    driver.quit()