import time

import allure
import pytest

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.yaml_management import read_yaml
from 企业微信web自动化.page.department_page import DepartmentPage


@allure.feature('通讯录模块')
@allure.story('部门管理')
class TestDepartment(object):
    @allure.title('添加部门，正确录入信息')
    @pytest.mark.parametrize('department_name', read_yaml('../data/department.yaml')['success'])
    def test_add_department(self, browser, department_name):
        department = DepartmentPage(browser)
        department.open_url()
        time.sleep(1)
        with allure.step('录入部门信息，判断部门是否添加成功'):
            result = department.add_department(department_name).check_department()
            log().info('断言部门是否添加成功')
            assert department_name in result
