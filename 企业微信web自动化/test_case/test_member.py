import allure
import pytest

from 企业微信web自动化.base.base_log import log
from 企业微信web自动化.base.yaml_management import read_yaml
from 企业微信web自动化.page.home_page import HomePage


@allure.feature('通讯录模块')
@allure.story('成员管理')
class TestMember(object):
    @allure.title('添加成员，录入成员姓名、账号、电话号码')
    @pytest.mark.parametrize('username,account,tel', read_yaml('../data/member.yaml')['success'])
    def test_member(self, browser, username, account, tel):
        home = HomePage(browser)
        home.open_url()
        with allure.step(f'添加成员,成员名称为:{username},成员账号为:{account},成员电话为:{tel}，判断成员是否添加成功'):
            result = home.goto_member().add_member(username, account, tel).check_member()
            log().info('断言添加成员是否成功')
            assert username in result
